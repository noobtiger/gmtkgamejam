import React from 'react';
import Game from './containers/Game';
import { StoreProvider } from 'easy-peasy';
import { store } from './store';

function App() {
  return (
    <StoreProvider store={store}>
      <Game />
    </StoreProvider>
  );
}

export default App;
