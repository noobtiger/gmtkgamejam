import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { useStoreState } from '../../store';

const useStyles = makeStyles({
  root: {
    fontSize: '1.2em',
    textTransform: 'uppercase',
    padding: '0px',
  },
  statusContainer: {
    display: 'flex',
  },
  orderContainer: {
    flex: 1,
  },
  paddingContainer: {
    padding: '0 1em',
  }
});

export const Status = () => {
  const classes = useStyles();
  const rootClass = `nes-container is-centered ${classes.root}`;
  const day = useStoreState(store => store.gameState.day);
  const orders = useStoreState(store => store.gameState.orders);
  const drivers = useStoreState(store => store.gameState.drivers);
  const unfinishedOrders = orders.filter(order => order.status !== 'Delivered');
  return (
    <div className={rootClass}>
      <article className={classes.statusContainer}>
        <div className={classes.paddingContainer}>
          Day: {day}
        </div>
        <div className={classes.paddingContainer}>
          Drivers: {drivers.length}
        </div>
        <div className={classes.orderContainer}>
          Remaining Orders: {unfinishedOrders.length}
        </div>
      </article>
    </div>
  );
}
