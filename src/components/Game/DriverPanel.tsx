import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { useStoreState, useStoreActions } from '../../store';

const useStyles = makeStyles({
  root: {
    overflow: 'auto',
    height: '100%',
  },
  driverContainer: {

  },
  actionContainer: {
    display: 'flex'
  },
  badgeContainer: {
    top: '0px',
    right: '0px',
    position: 'absolute',
    fontSize: '0.7em',
  }
});

export const DriverPanel = () => {
  const classes = useStyles();
  const drivers = useStoreState(state => state.gameState.drivers);
  const deliverNextOrder = useStoreActions(state => state.gameState.deliverNextOrder);
  const takeBreak = useStoreActions(state => state.gameState.takeBreak);
  const driverElements = drivers.map((driver) => {
    const { id, name, energy, status } = driver;
    const handleClick = () => {
      deliverNextOrder(id);
    };
    const handleBreakClick = () => {
      takeBreak(id);
    }
    return (
      <div key={id} className={`nes-container ${classes.driverContainer}`}>
        {name}
        <div className={`nes-badge ${classes.badgeContainer}`}>
          <span className={status === 'Idle' ? 'is-dark' : 'is-warning'}>{status}</span>
        </div>
        <progress className="nes-progress is-success" value={energy} max="100"></progress>
        <div className={classes.actionContainer}>
          <button type="button" className={`nes-btn ${(status === 'Idle') ? (energy <= 0 ? 'is-disabled' : '') : 'is-disabled'}`} onClick={handleClick}>Deliver</button>
          <button type="button" className={`nes-btn ${status === 'Idle' ? '' : 'is-disabled'}`} onClick={handleBreakClick}>Take break</button>
        </div>
      </div>
    );
  });
  return (
    <div className={classes.root}>
      {driverElements}
    </div>
  );
}
