import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { useStoreState } from '../../store';

const useStyles = makeStyles({
  root: {
    overflow: 'auto',
    height: '100%',
  },
  orderContainer: {
    position: 'relative',
    padding: 20,
  },
  badgeContainer: {
    top: '0px',
    right: '0px',
    position: 'absolute',
    fontSize: '0.7em',
  }
});

export const OrderPanel = () => {
  const classes = useStyles();
  const orders = useStoreState(state => state.gameState.orders);
  const orderElements = orders.filter(order => order.status !== 'Delivered').map(order => {
    const { id, name, time, status } = order; 
    return (
      <div key={id} className={`nes-container ${classes.orderContainer}`}>
        {name}
        <div>Time: {time}</div>
        <div className={`nes-badge ${classes.badgeContainer}`}>
          <span className={status === 'InDelivery' ? 'is-warning' : 'is-dark'}>{status}</span>
        </div>
      </div>
    );
  });
  return (
    <div className={`${classes.root}`}>
      {orderElements}
    </div>
  );
}
