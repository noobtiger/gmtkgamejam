import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { useStoreState, useStoreActions } from '../../store';
import { endTimer } from '../../store/timerUtils';

const useStyles = makeStyles({
  root: {
    position: 'relative',
  },
  calendarDividerContainer: {
    padding: '20px',
    display: 'flex',
    alignItems: 'center',
    zIndex: 9,
  },
  calendarLine: {
    width: '100%',
    height: '1px',
    border: '1px solid black',
    zIndex: 9,
  },
  selectionDivider: {
    width: '100%',
    height: '1px',
    border: '1px solid red',
    position: 'absolute',
    left: '0px',
    zIndex: 10,
  },
  deliveryContainer: {
    backgroundColor: '#209cee',
    position: 'absolute',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    fontSize: '0.7em',
    alignItems: 'center',
    border: '1px solid lightgray',
    opacity: 0.7,
  },
  deliveryDetailContainer: {
    width: '70%',
    zIndex: 10,
  },
  breakContainer: {
    backgroundColor: '#d3d3d3',
    position: 'absolute',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    fontSize: '0.7em',
    alignItems: 'center',
  }
});

export const CalendarPanel = () => {
  const classes = useStyles();
  const timer = useStoreState(state => state.gameState.timer);
  const deliveries = useStoreState(state => state.gameState.delivery);
  const orders = useStoreState(state => state.gameState.orders);
  const drivers = useStoreState(state => state.gameState.drivers);
  const breaks = useStoreState(state => state.gameState.break);
  const [ height, setHeight ] = React.useState(0);

  // const startDay = useStoreActions(state => state.gameState.startDay);
  // React.useEffect(() => {
  //   startDay();
  // }, []);
  
  const calendarContainerRef = React.useCallback(node => {
    if (node !== null) {
      setHeight(node.getBoundingClientRect().height);
    }
  }, []);
  const calendarDividers = new Array(8).fill(0).map((_el, index) => {
    return (
      <div className={classes.calendarDividerContainer} key={index}>
        <div>
          {index + 1}
        </div>
        <div className={classes.calendarLine}>
        </div>
      </div>
    )
  });
  const topDistance = (height/120) * timer;
  const currentTimerLine = <div className={classes.selectionDivider} style={{ top: topDistance }}></div>;

  const deliveriesMap = deliveries.map(delivery => {
    const { orderId, driverId, startTime, endTime } = delivery;
    const deliveryTop = (height/120) * startTime;
    const deliveryHeight = (endTime * (height/120)) - deliveryTop;
    const currentOrder = orders.find(order => order.id === orderId);
    const currentDriver = drivers.find(driver => driver.id === driverId);
    return (
      <div key={`${orderId}_${driverId}`} className={classes.deliveryContainer} style={{ top: deliveryTop, height: deliveryHeight }}>
        <div className={`nes-badge is-splited ${classes.deliveryDetailContainer}`}>
          <span className="is-dark">{currentOrder?.name}</span>
          <span className="is-success">{currentDriver?.name}</span>
        </div>
      </div>
    );
  });

  const breakElements = breaks.map(currentBreak => {
    const { id, driverId, startTime, endTime, } = currentBreak;
    const deliveryTop = (height/120) * startTime;
    const deliveryHeight = (endTime * (height/120)) - deliveryTop;
    const currentDriver = drivers.find(driver => driver.id === driverId);
    return (
      <div key={id} className={classes.breakContainer} style={{ top: deliveryTop, height: deliveryHeight }}>
        <div className={`nes-badge is-splited ${classes.deliveryDetailContainer}`}>
          <span className="is-success">{currentDriver?.name}</span>
        </div>
      </div>
    );
  });
  return (
    <div className={classes.root} ref={calendarContainerRef}>
      {calendarDividers}
      {currentTimerLine}
      {deliveriesMap}
      {breakElements}
    </div>
  );
}
