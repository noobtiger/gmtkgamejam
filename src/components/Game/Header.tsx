import React from 'react';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
  root: {
    fontSize: '1.2em',
    textTransform: 'uppercase',
  },
  amazonColor: {
    color: '#FF9900',
    fontWeight: 900,
  },
});

export const Header = () => {
  const classes = useStyles();
  const rootClass = `nes-container is-centered ${classes.root}`;
  return (
    <div className={rootClass}>
      <span className={classes.amazonColor}>Amijan</span> delivery system
    </div>
  );
}
