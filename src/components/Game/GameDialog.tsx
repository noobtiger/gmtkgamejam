import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { useStoreState, useStoreActions } from '../../store';

const useStyles = makeStyles({
  root: {
    zIndex: 100,
    position: 'absolute',
    display: 'flex',
    top: '20%',
    width: '80%',
  },
  backdropContainer: {
    position: 'absolute',
    top: '0px',
    left: '0px',
    width: '100%',
    height: '100%',
    zIndex: 99,
    backgroundColor: 'gray',
    opacity: 0.5,
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'center',
  }
});

export const GameDialog = () => {
  const classes = useStyles();
  const gameStatus = useStoreState(state => state.gameState.gameStatus);
  const startDay = useStoreActions(state => state.gameState.startDay);
  const orders = useStoreState(state => state.gameState.orders);
  const days = useStoreState(state => state.gameState.day);
  const handleGameStart = () => {
    startDay();
  };
  if (gameStatus === 'inprogress') {
    return <div />;
  }
  const content = gameStatus === 'startmenu' && (
    <div>
      <p>Welcome to amijan delivery system.</p>
      <p>As a delivery manager, your task is to assign drivers to deliver certain number of orders in a day.</p>
      <p>If you miss an order, you will be fired.</p>
    </div>
  );
  const nextDayContent = gameStatus === 'dayended' && (
    <div>
      <p>You successfully delivered {orders.length} orders.</p>
    </div>
  );
  const gameOverContent = gameStatus === 'gameover' && (
    <div>
      <p>You survived {days} days.</p>
    </div>
  );
  return (
    <section>
      <dialog className={`nes-dialog is-rounded ${classes.root}`}>
        <form method="dialog">
          {content || nextDayContent || gameOverContent}
          <menu className={`dialog-menu ${classes.buttonContainer}`}>
            <button className="nes-btn is-primary" onClick={handleGameStart}>
              {gameStatus === 'startmenu' ? 'Start' : (gameStatus === 'dayended' ? 'Next day' : 'Start again')}
            </button>
          </menu>
        </form>
      </dialog>
      <div className={classes.backdropContainer}></div>
    </section>
  );
}
