import { Action, action, Thunk, thunk } from 'easy-peasy';
import { startTimer, endTimer, getDrivers, getOrders } from './timerUtils';
import { StoreModel } from '.';

interface Driver {
  id: string;
  name: string;
  energy: number;
  status: 'Break' | 'Deliver' | 'Idle';
}

interface Order {
  id: string;
  name: string;
  time: number;
  status: 'Delivered' | 'Warehouse' | 'InDelivery';
}

interface Delivery {
  id: string;
  orderId: string;
  driverId: string;
  startTime: number;
  endTime: number;
}

interface Break {
  id: string;
  driverId: string;
  startTime: number;
  endTime: number;
}

interface ChangeOrder {
  order: Order;
  orderId: string;
}

interface ChangeDriver {
  driver: Driver;
  driverId: string;
}

type GameStatus = 'startmenu' | 'inprogress' | 'dayended' | 'gameover';

export interface GameStateModel {
  gameStatus: GameStatus;
  updateGameStatus: Action<GameStateModel, GameStatus>;
  day: number;
  updateDate: Action<GameStateModel, number>;
  orders: Order[];
  setOrders: Action<GameStateModel, Order[]>;
  updateOrder: Action<GameStateModel, ChangeOrder>;
  drivers: Driver[];
  setDrivers: Action<GameStateModel, Driver[]>;
  updateDriver: Action<GameStateModel, ChangeDriver>;
  delivery: Delivery[];
  addDelivery: Action<GameStateModel, Delivery>;
  updateDelivery: Action<GameStateModel, Delivery[]>;
  break: Break[]; 
  addBreak: Action<GameStateModel, Break>;
  updateBreak: Action<GameStateModel, Break[]>;
  timer: number;
  setTimer: Action<GameStateModel, number>;
  updateTimer: Thunk<GameStateModel, number, {}, StoreModel>;
  startDay: Thunk<GameStateModel>;
  endDay: Thunk<GameStateModel>;
  deliverNextOrder: Thunk<GameStateModel, string, {}, StoreModel>;
  takeBreak: Thunk<GameStateModel, string, {}, StoreModel>;
}

const gameState: GameStateModel = {
  gameStatus: 'startmenu',
  updateGameStatus: action((state, payload) => {
    state.gameStatus = payload;
  }),
  day: 0,
  updateDate: action((state, payload) => {
    state.day = payload;
  }),
  orders: [],
  setOrders: action((state, payload) => {
    state.orders = payload;
  }),
  updateOrder: action((state, payload) => {
    const { order: updatedOrder, orderId } = payload;
    state.orders = [...state.orders].map((order) => {
      if (order.id === orderId) {
        return updatedOrder;
      }
      return order;
    })
  }),
  drivers: [],
  setDrivers: action((state, payload) => {
    state.drivers = payload;
  }),
  updateDriver: action((state, payload) => {
    const { driver: updatedDriver, driverId } = payload;
    state.drivers = [...state.drivers].map((order) => {
      if (order.id === driverId) {
        return updatedDriver;
      }
      return order;
    });
  }),
  delivery: [],
  addDelivery: action((state, payload) => {
    state.delivery = [...state.delivery, payload]
  }),
  updateDelivery: action((state, payload) => {
    state.delivery = payload;
  }),
  break: [],
  addBreak: action((state, payload) => {
    state.break = [...state.break, payload]
  }),
  updateBreak: action((state, payload) => {
    state.break = payload;
  }),
  timer: 0,
  setTimer: action((state, payload) => {
    state.timer = payload;
  }),
  updateTimer: thunk((actions, payload, helpers)  => {
    const { delivery, orders, drivers, break: breaks, gameStatus } = helpers.getState();
    const finishedDeliveries = delivery.filter(current => current.endTime <= payload);
    // const unFinishedDeliveries = delivery.filter(current => current.endTime > payload);

    finishedDeliveries.forEach(finishedDelivery => {
      const{ orderId, driverId } = finishedDelivery;
      const orderIndex = orders.findIndex(order => order.id === orderId);
      const driverIndex = drivers.findIndex(driver => driver.id === driverId);
      const currentOrder = orders[orderIndex];
      const currentDriver = drivers[driverIndex];
      if (currentOrder.status !== 'Delivered') {
        actions.updateOrder({ order: { ...currentOrder, status: 'Delivered' }, orderId: currentOrder.id});
        actions.updateDriver({ driver: { ...currentDriver, status: 'Idle' }, driverId: currentDriver.id});
      }
    });
    
    const finishedBreaks = breaks.filter(current => current.endTime <= payload);
    finishedBreaks.forEach(currentBreak => {
      const { driverId } = currentBreak;
      const driverIndex = drivers.findIndex(driver => driver.id === driverId);
      const currentDriver = drivers[driverIndex];
      if (currentDriver.status === 'Break') {
        actions.updateDriver({ driver: { ...currentDriver, status: 'Idle' }, driverId: currentDriver.id});
      }
    });

    const unfinishedOrders = orders.filter(order => order.status === 'Warehouse' || order.status === 'InDelivery');
    if ((unfinishedOrders.length === 0 || payload >= 120) && gameStatus === 'inprogress') {
      actions.endDay();
    }
    // actions.updateDelivery(unFinishedDeliveries);
    actions.setTimer(payload);
  }),
  startDay: thunk((actions, payload, helpers) => {
    const { day, gameStatus } = helpers.getState();
    if (gameStatus === 'gameover') {
      actions.updateDate(0);
    }
    actions.updateTimer(0);
    actions.updateDate(day + 1);
    actions.updateDelivery([]);
    actions.updateBreak([]);
    startTimer();
    const orders = getOrders(day);
    actions.setOrders(orders as Order[]);
    const driverCount = getDrivers(day);
    const drivers = new Array(driverCount).fill(0).map((driver, index) => {
      return {
        id: `${index+1}`,
        name: `Driver # ${index + 1}`,
        energy: 100,
        status: 'Idle',
      };
    });
    actions.setDrivers(drivers as Driver[]);
    actions.updateGameStatus('inprogress');
  }),
  endDay: thunk((actions, payload, helpers) => {
    const { orders } = helpers.getState();
    const unfinishedOrders = orders.filter(order => order.status === 'Warehouse' || order.status === 'InDelivery');
    if (unfinishedOrders.length > 0) {
      actions.updateGameStatus('gameover');
    } else {
      actions.updateGameStatus('dayended');
    }

    endTimer();
  }),
  deliverNextOrder: thunk((actions, payload, helpers) => {
    const { orders, drivers, timer } = helpers.getState();
    const firstOrderIndex = orders.findIndex(order => order.status === 'Warehouse');
    const firstOrder = orders[firstOrderIndex];
    const currentDriver = drivers.find(driver => driver.id === payload);
    const updateOrderPayload = {
      order: {
        ...firstOrder,
        status: 'InDelivery' as 'InDelivery',
      },
      orderId: firstOrder.id,
    }
    actions.updateOrder(updateOrderPayload);
    if (currentDriver) {
      const updateDriverPayload = {
        driver: {
          ...currentDriver,
          status: 'Deliver' as 'Deliver',
          energy: (currentDriver?.energy || 0) - firstOrder.time * 3,
        },
        driverId: currentDriver?.id || '',
      };
      actions.updateDriver(updateDriverPayload);
    }
    const newDelivery = {
      id: `${Date.now()}`,
      orderId: firstOrder.id,
      driverId: currentDriver?.id || '',
      startTime: timer,
      endTime: firstOrder.time  + timer,
    };
    actions.addDelivery(newDelivery);
  }),
  takeBreak: thunk((actions, payload, helpers) => {
    const { drivers, timer } = helpers.getState();
    const currentDriver = drivers.find(driver => driver.id === payload);
    if (currentDriver) {
      const updateDriverPayload = {
        driver: {
          ...currentDriver,
          status: 'Break' as 'Break',
          energy: 100,
        },
        driverId: payload,
      };
      actions.updateDriver(updateDriverPayload);
    }
    const newBreak = {
      id: `${Date.now()}`,
      driverId: currentDriver?.id || '',
      startTime: timer,
      endTime: timer + 20,
    };
    actions.addBreak(newBreak);
  })
};

export default gameState;