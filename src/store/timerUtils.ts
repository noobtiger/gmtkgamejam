import { store } from './';
import { ThunkCreator } from 'easy-peasy';

let timer: number | null = null;
let startTime: number | null = null;
let updateTimer:  ThunkCreator<number, null> | null= null;

export const startTimer = () => {
  timer = window.setInterval(() => {
    if (updateTimer === null) {
      updateTimer = store.getActions().gameState.updateTimer;
    }
    if (startTime === null) {
      startTime = Date.now();
    } else {
      const currentDate = Date.now();
      updateTimer(Math.round(((currentDate - startTime)/1000)));
    }
  }, 1000);
};

export const endTimer = () => {
  if (timer !== null) {
    window.clearInterval(timer);
  }
  timer = null;
  if (updateTimer !== null) {
    updateTimer(0);
  }
  startTime = null;
};

export const getDrivers = (day: number) => {
  if (day < 2) {
    return 1;
  } else if (day < 10) {
    return 5;
  } else {
    return 10;
  }
};

export const getOrders = (day: number) => {
  const randomTimes = [10, 15, 20, 25, 30];
  const orderCount = day < 2 ? Math.floor(Math.random() * 2 * day) + 1 : Math.floor(Math.random() * 3 * day) + 3 * day;
  const ordersCount = new Array(orderCount).fill(0).map((_, index) => {
    const randomTimeIndex = Math.floor(Math.random() * 5);
    return  {
      id: `${index+1}`,
      name: `Order # ${index + 1}`,
      time: randomTimes[randomTimeIndex],
      status: 'Warehouse',
    }
  });
  return ordersCount;
};