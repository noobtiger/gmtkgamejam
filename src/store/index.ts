import { createStore, createTypedHooks } from 'easy-peasy';
import gameState, { GameStateModel } from './gameState';

export interface StoreModel {
  gameState: GameStateModel;
}

const model: StoreModel = {
  gameState,
};

export const store = createStore(model);

const { useStoreActions, useStoreState, useStoreDispatch } = createTypedHooks<StoreModel>();

export { useStoreActions, useStoreState, useStoreDispatch };
