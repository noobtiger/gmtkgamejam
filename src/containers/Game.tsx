import React from 'react';
import { Header, DriverPanel, Status, CalendarPanel, OrderPanel, GameDialog } from '../components';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
  container: {
    display: 'flex',
    height: 'calc(100vh - 120px)'
  },
  borderContainer: {
    borderColor: '#000',
    borderStyle: 'solid',
    borderWidth: '4px',
  },
  orderPanel: {
    width: '350px',
  },
  driverPanel: {
    width: '350px',
  },
  calendarPanel: {
    flex: 1,
  }
});

function Game() {
  const classes = useStyles();
  return (
    <main>
      <GameDialog />
      <Header />
      <Status />
      <section className={classes.container}>
        <article className={`${classes.calendarPanel} ${classes.borderContainer}`}>
          <CalendarPanel />
        </article>
        <article className={`${classes.orderPanel} ${classes.borderContainer}`}>
          <OrderPanel />
        </article>
        <article className={`${classes.driverPanel} ${classes.borderContainer}`}>
          <DriverPanel />
        </article>
      </section>
    </main>
  );
}

export default Game;
